<?php
use App\Autoloader;
use App\Core\Router;

define('ROOT', dirname(__DIR__));
define('VIEWS', ROOT . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'Views' . DIRECTORY_SEPARATOR);
define('SCRIPTS', dirname($_SERVER['SCRIPT_NAME']));

require '../autoloader.php';

Autoloader::register();
$url = isset($_GET['url']) ? $_GET['url'] : '';
$router = new Router($url);
$router->get('/', 'ProduitsController@index');
$router->get('/produit/:id', 'ProduitsController@lireUn');
$router->get('/categorie/:name', 'CategoriesController@lireUn');
$router->run();