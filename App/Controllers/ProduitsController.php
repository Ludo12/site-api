<?php
namespace App\Controllers;

class ProduitsController extends Controller
{
    public function index()
    {
        $data = json_decode(file_get_contents("http://localhost:3000/produit/read"));
        $produits = $data->produits;
        $this->render('produit.index', compact('produits'));
    }

    public function lireUn(int $id)
    {
        $id = htmlspecialchars(strip_tags($id));
        $produit = json_decode(file_get_contents("http://localhost:3000/produit/read/" . $id));
        $this->render('produit.detail', compact('produit'));
    }
}