<?php
namespace App\Controllers;

class CategoriesController extends Controller
{
    public function lireUn(string $name)
    {
        $name = ucfirst(htmlspecialchars(strip_tags($name)));
        $data = json_decode(file_get_contents("http://localhost:3000/categorie/read/" . $name));

        $error = (isset($data->message)) ? $data->message : '';
        $prodByCategories = (isset($data->categories)) ? $data->categories : '';

        $this->render('categorie.index', compact('error', 'prodByCategories', 'name'));
    }
}