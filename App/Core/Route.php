<?php
namespace App\Core;

class Route
{
    public $path;
    public $action;
    public $matches;

    public function __construct($path, $action)
    {
        $this->path = trim($path, '/');
        $this->action = $action;
    }

    /**
     * matche l'url avec le path de la route
     *
     * @param  string $url chemin a vérifier
     * @return bool
     */
    public function matches(string $url): bool
    {
        //On remplace dans $this->path tout ce qui commence par ":" et
        //qui serait après un caractère alpha numèrique par tout sauf "/"
        //Si une correspondance est trouvée avec le path on recupère le paramètre
        $path = preg_replace('#:([\w]+)#', '([^/]+)', $this->path);
        //On crée un nouveau pattern
        $pathPattern = "#^$path$#";
        //On cherche une correspondance de $pathPattern dans l'url et on stock dans $matches
        if (preg_match($pathPattern, $url, $matches)) {
            $this->matches = $matches;
            return true;
        } else {
            return false;
        }
    }

    /**
     * execute l'action en fonction du controller et de la methode
     *
     * @return void
     */
    public function execute()
    {
        //On explose action pour avoir un tableau avec le controlleur et la fonction
        $params = explode('@', $this->action);

        //On cré une nouvelle instance du controller
        $file = "\\App\\Controllers\\" . $params[0];
        $controller = new $file;

        //On génère la methode
        $method = $params[1];

        //On vérifie s'il existe un paramètre dans $this->matches et on retourne la fonction correspondante
        return isset($this->matches[1]) ? $controller->$method($this->matches[1]) : $controller->$method();
    }
}