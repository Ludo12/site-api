<h1><?=$name?></h1>
<?php
if (!empty($error)):
    printf('%s %s %s', "<p>", $error, "</p>");
else:
?>
<table class="table">
    <thead>
        <tr class="table-secondary">
            <th scope="col">Catégorie</th>
            <th scope="col">Nom</th>
            <th scope="col">Description</th>
            <th scope="col">Prix</th>
        </tr>
    </thead>
    <tbody>
        <?php
foreach ($prodByCategories as $prod_cat):
?>
        <tr>
            <th scope="row"><?=$prod_cat->categorie?></th>
            <td><a href="/produit/<?=$prod_cat->id?>"><?=$prod_cat->nom?></a></td>
            <td><?=$prod_cat->description?></td>
            <td><?=$prod_cat->prix?> €</td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
<?php endif;?>