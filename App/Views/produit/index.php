<h1>Les produits</h1>
<table class="table">
    <thead>
        <tr class="table-secondary">
            <th scope=" col">#</th>
            <th scope="col">Nom</th>
            <th scope="col">Description</th>
            <th scope="col">Prix</th>
            <th scope="col">ID Categorie</th>
            <th scope="col">Nom Categorie</th>
        </tr>
    </thead>
    <tbody>
        <?php
foreach ($produits as $produit):
?>
        <tr>
            <th scope="row"><?=$produit->id?></th>
            <td><a href="/produit/<?=$produit->id?>"><?=$produit->nom?></a></td>
            <td><?=$produit->description?></td>
            <td><?=$produit->prix?> €</td>
            <td><?=$produit->categories_id?></td>
            <td><a href="/categorie/<?=$produit->categories_nom?>"><?=$produit->categories_nom?></a></td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>