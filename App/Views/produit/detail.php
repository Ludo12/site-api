<div class="card text-white bg-dark my-3" style="max-width: 18rem;">
    <div class="card-header"><?=$produit->nom?></div>
    <div class="card-body">
        <h5 class="card-title"><?=$produit->nom?> | <?=$produit->categories_nom?></h5>
        <p class="card-text"><?=$produit->description?></p>
        <p class="card-text"><?=$produit->prix?> €</p>
    </div>
</div>