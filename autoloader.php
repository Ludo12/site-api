<?php
namespace App;

class Autoloader
{

    public static function register()
    {
        /**
         * Permet d'enregistrer une fonction qui sera appelée à chaque fois
         * qu'une classe inconnue sera rencontrée.
         */
        spl_autoload_register([
            //récupére la classe par la constante magique "__CLASS__" et et déclenche la méthode "autoload"
            __CLASS__,
            'autoload',
        ]);}

    public static function autoload($class)
    {
        //On retire le début du namespace "App" puis on remplace les "\" par des "/"
        //pour créer le chemin du fichier.
        $class = str_replace(__NAMESPACE__ . '\\', '', $class);
        $class = str_replace('\\', '/', $class);

        if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . $class . '.php')) {
            require __DIR__ . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . $class . '.php';
        }
    }

}